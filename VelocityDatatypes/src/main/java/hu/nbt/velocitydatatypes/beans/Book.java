package hu.nbt.velocitydatatypes.beans;

public class Book {
    
    private final Long id;
    private final String title;
    private final String author;

    public Book(String title, String author) {
        this.id = null;
        this.title = title;
        this.author = author;
    }

    public Book(Long id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public Long getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }
    
    public static class Builder {
    
        private Long id;
        private String title;
        private String author;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }
        
        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setAuthor(String author) {
            this.author = author;
            return this;
        }

        public Book build() {
            Book book = new Book(
                    this.id,
                    this.title, 
                    this.author);
            return book;
        }
    }
}

