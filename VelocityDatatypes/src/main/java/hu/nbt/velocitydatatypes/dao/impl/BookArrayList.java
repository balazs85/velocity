package hu.nbt.velocitydatatypes.dao.impl;

import hu.nbt.velocitydatatypes.beans.Book;
import hu.nbt.velocitydatatypes.dao.BookDao;
import java.util.ArrayList;
import java.util.List;

public class BookArrayList implements BookDao{
    
    private List<Book> books;

    public BookArrayList() {
        books = new ArrayList<>();
        
        books.add(new Book(1L, "Egri Csillagok", "Gárdonyi Géza"));
        books.add(new Book(2L, "Elvész a nyom", "Wass Albert"));
        books.add(new Book(3L, "Eliza", "Wass Albert"));
    }

    @Override
    public List<Book> getBooks() {
        return books;
    }

    @Override
    public void addBook(Book book) {
        books.add(book);
    }
    
}
