package hu.nbt.velocitydatatypes.dao;

import hu.nbt.velocitydatatypes.beans.Book;
import java.util.List;

public interface BookDao {
    public List<Book> getBooks();
    public void addBook(Book book);
}
