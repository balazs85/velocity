package hu.nbt.velocitypersisthibernate.dao;

import hu.nbt.velocitydatatypes.beans.Book;
import hu.nbt.velocitydatatypes.dao.BookDao;
import hu.nbt.velocitypersisthibernate.entity.BookEntity;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class BookHibernateDao implements BookDao {

    public BookHibernateDao() {
        super();

        addBook(new Book("Egri Csillagok", "Gárdonyi Géza"));
        addBook(new Book("Elvész a nyom", "Wass Albert"));
        addBook(new Book("Eliza", "Wass Albert"));
    }

    @Override
    public List<Book> getBooks() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List<Book> books = new ArrayList<>();
        List<BookEntity> bookEntitys = null;
        
        try {
            session.beginTransaction();
            Criteria cr = session.createCriteria(BookEntity.class);
            bookEntitys = cr.list();
            for (BookEntity bookEntity : bookEntitys) {
                Book book = mapBookEntityToBook(bookEntity);
                books.add(book);
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public void addBook(Book book) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        BookEntity bookEntity = mapBookToBookEntity(book);
        session.save(bookEntity);

        session.getTransaction().commit();
    }
    
    public BookEntity mapBookToBookEntity(Book book) {
        BookEntity bookEntity = new BookEntity();
        
        bookEntity.setId(book.getId());
        bookEntity.setAuthor(book.getAuthor());
        bookEntity.setTitle(book.getTitle());
        
        return bookEntity;
    }

    public Book mapBookEntityToBook(BookEntity bookEntity) {
        Book book = new Book.Builder()
                .setId(bookEntity.getId())
                .setAuthor(bookEntity.getAuthor())
                .setTitle(bookEntity.getTitle())
                .build();
        
        return book;
    }

}
