/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.nbt.velocityhello.servlet;

import hu.nbt.velocitydatatypes.dao.BookDao;
import hu.nbt.velocitydatatypes.dao.impl.BookArrayList;
import hu.nbt.velocitypersisthibernate.dao.BookHibernateDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "BookServlet", urlPatterns = {"/bookTable.html"})
public class BookServlet extends HttpServlet {
    
    BookDao dao = new BookHibernateDao();
    //BookDao persistDao = new BookHibernateDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {            
            request.setAttribute("books", dao.getBooks());
            RequestDispatcher requestDispatcher =  request.getRequestDispatcher("bookTable.vm");
            requestDispatcher.forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
